(require 'cljs.closure)

(cljs.closure/build
 "src"
 {:main      'test.core
  :output-to "main.js"
  :target    :nodejs})
