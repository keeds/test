(ns test.core
  (:require [cljs.nodejs     :as nodejs]
            [cljs.core.async :refer [chan timeout put! <!]]
            [cljs.test       :as test :refer-macros [deftest testing is async]]
            [clojure.browser.repl :as repl]
            )
  (:require-macros [cljs.core.async.macros :refer [go]])
  )

;; (defonce conn
;;   (repl/connect "http://localhost:9000/repl"))

;; (enable-console-print!)

;; (println "Hello world!")

(nodejs/enable-util-print!)

(defn remote [x]
  (let [c (chan)]
    (go
      (<! (timeout 2000))
      (put! c x))
    c))

(deftest base
  (cljs.test/async done
                   (go
                     (is (= 1 3))
                     (is (= 1 (<! (remote 2))))
                     (done))
                   ))

(deftest next-test
  (is (= :a :b)))

(defn -main [& args]
  (test/run-tests))

(set! *main-cli-fn* -main)
