LEINCP=cljs.jar:$(shell lein classpath)

compile: out/main.js main.js

main.js: src/test/core.cljs cljs.jar
	java -cp $(LEINCP) clojure.main node.clj

out/main.js: src/test/core.cljs cljs.jar
	java -cp $(LEINCP) clojure.main build.clj

cljs.jar:
	curl -OL 'https://github.com/clojure/clojurescript/releases/download/r3126/cljs.jar'

.PHONY: clean

clean:
	rm -f main.js out/main.js

deps:
	lein deps
